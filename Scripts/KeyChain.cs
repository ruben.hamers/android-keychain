using System;

namespace HamerSoft.AndroidKeyChain
{
    public class KeyChain : IDisposable
    {
        public KeyStoreAccess KeyStore { get; private set; }
        public SharedPreferencesAccess SharedPreferences { get; private set; }
        public PasswordManager PasswordManager { get; private set; }

        public KeyChain(string alias, string sharedPreferencesKey)
        {
            KeyStore = new KeyStoreAccess(new object[] {alias});
            SharedPreferences = new SharedPreferencesAccess(new object[] {alias});
            PasswordManager = new PasswordManager(new object[] {alias,sharedPreferencesKey});
        }

        public void Dispose()
        {
            KeyStore = null;
            SharedPreferences = null;
            PasswordManager = null;
        }
    }
}