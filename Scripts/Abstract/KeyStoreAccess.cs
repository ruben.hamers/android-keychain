using System.Collections.Generic;
using UnityEngine;

namespace HamerSoft.AndroidKeyChain
{
    public class KeyStoreAccess : AbstractAndroidInterop
    {
        public KeyStoreAccess(params object[] objects) : base(objects)
        {
        }

        protected override string AndroidClassName => "KeyStoreAccess";

        public List<string> GetKeyAliases()
        {
            return Instance.Call<List<string>>("getKeyAliases");
        }

        public void CreateKeys(string alias)
        {
            Instance.Call("createKeys", new object[] {alias});
        }

        public string Encrypt(string plainText)
        {
            return Instance.Call<string>("encrypt", new object[] {plainText});
        }

        public string Encrypt(string plainText, string alias)
        {
            return Instance.Call<string>("encrypt", new object[] {plainText, alias});
        }

        public string Decrypt(string encryptedString)
        {
            return Instance.Call<string>("decrypt", new object[] {encryptedString});
        }

        public string Decrypt(string encryptedString, string alias)
        {
            return Instance.Call<string>("decrypt", new object[] {encryptedString, alias});
        }

        public void Delete()
        {
            Instance.Call("delete");
        }

        public void Delete(string alias)
        {
            Instance.Call("delete", new object[] {alias});
        }
    }
}