using System.Collections.Generic;
using UnityEngine;

namespace HamerSoft.AndroidKeyChain
{
    public abstract class AbstractAndroidInterop
    {
        private const string ANDROID_UNITY_PLAYER = "com.unity3d.player.UnityPlayer";
        private const string ACTIVITY_NAME = "currentActivity";
        private const string PACKAGE_NAME = "com.hamersoft.keychain.";
        protected AndroidJavaObject Instance;
        protected abstract string AndroidClassName { get; }

        protected AbstractAndroidInterop(params object[] constructorArguments)
        {
            CreateInstance(AndroidClassName, constructorArguments);
        }

        private void CreateInstance(string className, params object[] constructorArguments)
        {
            AndroidJavaClass unityPlayerClass = new AndroidJavaClass(ANDROID_UNITY_PLAYER);
            AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>(ACTIVITY_NAME);
            var arguments = new List<object> {unityActivity};
            if (constructorArguments != null)
                arguments.AddRange(constructorArguments);
            Instance = new AndroidJavaObject($"{PACKAGE_NAME}{className}", arguments.ToArray());
        }
    }
}