using UnityEngine;

namespace HamerSoft.AndroidKeyChain
{
    public class PasswordManager : AbstractAndroidInterop
    {
        protected override string AndroidClassName => "PasswordManager";

        public PasswordManager(params object[] objects) : base(objects)
        {
        }

        public bool SavePassword(string password)
        {
            bool saved = Instance.Call<bool>("savePassword", new object[] {password});
            Debug.Log($"Saved password {saved} with PW {password}");
            return saved;
        }

        public bool DeletePassword()
        {
            bool deleted = Instance.Call<bool>("deletePassword");
            return deleted;
        }

        public string GetPassword()
        {
            string password = Instance.Call<string>("getPassword");
            Debug.Log($"Got password {password}");
            return password;
        }
    }
}